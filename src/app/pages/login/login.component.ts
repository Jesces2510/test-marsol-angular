import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit() {
  }
  ngOnDestroy() {
  }


  login(e) {
    this.auth.login(e.value.user, e.value.pass).subscribe(res => {
      if(typeof res.access_token != 'undefined') {
        localStorage.setItem('dataUser', JSON.stringify(res));
        this.router.navigate(['/dashboard']);
      }
    })
  }
}
