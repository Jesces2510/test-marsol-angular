import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
} from "../../variables/charts";
import { MaintainerService } from 'src/app/services/maintainer.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public datasets: any;
  public data: any;
  public salesChart;
  public salesChart2;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public dataUser: any;
  public dataSalesByUser: any;
  public dataSalesTotal: any;

  constructor(private mainServ: MaintainerService) { }

  ngOnInit() {
    this.dataUser = JSON.parse(localStorage.getItem('dataUser'));
    parseOptions(Chart, chartOptions());


    let s = this.mainServ.getSalesByUser();
    s.snapshotChanges().subscribe(data => {
      this.dataSalesByUser = [];
      data.forEach(item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;
        this.dataSalesByUser.push(a);
      })

      let chart1 = chartExample1;
      chart1.data.datasets[0].data = [];
      chart1.data.labels = [];
      this.dataSalesByUser.forEach(item => {
        chart1.data.labels.push(item.fecha);
        chart1.data.datasets[0].data.push(item.cantidad);
      });

      console.log(chart1);  
      

      var chartSalesByUser = document.getElementById('chart-salesByUser');

      this.salesChart = new Chart(chartSalesByUser, {
        type: 'bar',
        options: chart1.options,
        data: chart1.data
      });
    })   
    
    let a = this.mainServ.getAllSales();
    a.snapshotChanges().subscribe(data => {
      this.dataSalesTotal = [];
      data.forEach(item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;
        this.dataSalesTotal.push(a);
      })
      console.log(this.dataSalesTotal);


      let labels = [];
      let dataSalesAll = [];
      this.dataSalesTotal.forEach(item => {
        labels.push(item.fecha);
        dataSalesAll.push(item.cantidad);
      });

      var chartSalesTotal = document.getElementById('chart-salesTotal');

      this.salesChart2 = new Chart(chartSalesTotal, {
        type: 'bar',
        options: {
          scales: {
            yAxes: [{
              ticks: {
                callback: function(value) {
                    return + value;
                }
              }
            }]
          }
        },
        data: {
          labels: labels,
          datasets: [{
            label: [],
            data: dataSalesAll
          }]
        }
      });
    }) 

  }
}
