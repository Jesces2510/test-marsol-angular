import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class MaintainerService {

  usuarios: AngularFireList<any>;
  items: AngularFireList<any>;
  ventasByUser: AngularFireList<any>;
  ventas: AngularFireList<any>;
  userLogged: any;

  constructor(private http: HttpClient, private db: AngularFireDatabase) {
    this.userLogged = JSON.parse(localStorage.getItem('dataUser'));
    this.ventasByUser = this.db.list('ventas',  ref => ref.orderByChild('vendedor').equalTo(this.userLogged.usuario));
    this.ventas = this.db.list('ventas');
   }



  getSalesByUser() {
    return this.ventasByUser;
  }

  getAllSales() {
    return this.ventas;
  }
  
}
