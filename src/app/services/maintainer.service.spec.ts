import { TestBed } from '@angular/core/testing';

import { MaintainerService } from './maintainer.service';

describe('MaintainerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaintainerService = TestBed.get(MaintainerService);
    expect(service).toBeTruthy();
  });
});
