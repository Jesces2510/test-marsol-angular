import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const endpoint = environment.baseUrlServices;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { 
    console.log(endpoint);
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  login(user: any, pass: any): Observable<any> {
    return this.http.get(endpoint + 'login', {
      params: {
        user: user,
        pass: pass
      }      
    })
  }
}
