// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrlServices: "https://marsol-test.herokuapp.com/",
  firebaseConfig: {
    apiKey: "AIzaSyB2bCxbOk5XVtcaE8L0Tw6egWHavp7c70Q",
    authDomain: "test-marsol.firebaseapp.com",
    databaseURL: "https://test-marsol.firebaseio.com",
    projectId: "test-marsol",
    storageBucket: "",
    messagingSenderId: "895888430318",
    appId: "1:895888430318:web:f7c633f362ddb9f0564fff",
    measurementId: "G-STBMYVSD4E"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
